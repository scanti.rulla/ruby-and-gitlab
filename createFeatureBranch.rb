gem 'gitlab'
require 'gitlab'

#Separator for words in the description
SEPARATOR = "-"


PROJECT_ID = 123456789
PRIVATE_TOKEN = ''
END_POINT = 'https://gitlab.com/api/v4'


##
# Converts the description of the issue in  a string that can be used as a GIT branch
def convert_issue_description( issue_description )

  newDescription = issue_description.strip.downcase.gsub(/([^a-z0-9])+/, SEPARATOR)
  newDescription = newDescription.gsub(/SEPARATOR SEPARATOR+/, "")

  newDescription.chop! if newDescription.end_with?(SEPARATOR)
  newDescription = newDescription[1, newDescription.length] if newDescription.start_with?(SEPARATOR)

  return newDescription
end

##
# Downloads the description of the issue
def download_issue_description(issue_id)

  g = Gitlab.client(
    endpoint: END_POINT,
    private_token: PRIVATE_TOKEN,
    httparty: {
      headers: { 'Cookie' => 'gitlab_canary=true' }
    }
  )

  issue = g.issue(PROJECT_ID, issue_id)

  return issue.title if issue != nil

  return nil
end

##
# Creates the branch locally and set the upstream
def create_branch(issue_id, desc)
  newBranch = issue_id.to_s() + SEPARATOR + convert_issue_description(desc)

  # I create the branch
  command = "git checkout -b " + newBranch + " master"
  puts command
  system(command)


  #I set the upstream
  command2 = "git branch --set-upstream origin " + newBranch
  puts command2
  system(command2)
end


issue_id = ARGV[0].to_i

desc = download_issue_description(issue_id)
create_branch(issue_id, desc)
