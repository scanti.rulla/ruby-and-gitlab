gem "minitest"
require "minitest/autorun"
require_relative "createFeatureBranch"


class DescriptionTest < Minitest::Test
  def test_convert1
    assert_equal 'invade-planet-earth', convert_issue_description('Invade planet Earth!!!'), "The description has not been converted"
  end

  def test_convert2
    assert_equal 'kill-all-the-aliens', convert_issue_description('@@@ Kill all the aliens! @@@'), "The description has not been converted"
  end

end
